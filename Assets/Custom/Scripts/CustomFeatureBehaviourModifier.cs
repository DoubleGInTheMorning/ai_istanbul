﻿namespace Mapbox.Unity.MeshGeneration.Modifiers
{
	using Mapbox.Unity.MeshGeneration.Data;
	using Mapbox.Unity.MeshGeneration.Components;
	using UnityEngine;
	using System.Collections.Generic;

	[CreateAssetMenu(menuName = "Mapbox/Modifiers/Add Feature Behaviour Modifier")]
	public class CustomFeatureBehaviourModifier : GameObjectModifier
	{
		private Dictionary<GameObject, CustomFeatureBehaviour> _features;        
		private CustomFeatureBehaviour _tempFeature;
        private static bool isShowingFeatures = false;
        public static List<CustomFeatureBehaviour> featureList;

        //TODO make this a dictionary for GO lookups
        public GameObject prefab;

        private static string searchFilter = "";

        public static void OnSearchFilter(string filter)
        {
            searchFilter = filter;
            ApplyFilter();
        }

        static void ApplyFilter()
        {
            if (searchFilter == "")
                return;

            foreach (CustomFeatureBehaviour feature in featureList)
            {
                ApplyFilter(feature);
            }
        }

        static void ApplyFilter(CustomFeatureBehaviour feature)
        {
            if (!feature.VectorEntity.Feature.Properties["name"].ToString().ToLower().Contains(searchFilter.ToLower()))
            {
                feature.gameObject.SetActive(false);
            }
            else
            {
                feature.gameObject.SetActive(true);
            }
        }

        public static void HideFeatures()
        {
            if (isShowingFeatures)
            {
                foreach (CustomFeatureBehaviour feature in featureList)
                {
                    feature.gameObject.SetActive(false);
                }
            }
            isShowingFeatures = false;
        }

        public static void ShowFeatures()
        {
            if(!isShowingFeatures)
                ApplyFilter();

            isShowingFeatures = true;
        }

		public override void Initialize()
		{
			if (_features == null)
			{
				_features = new Dictionary<GameObject, CustomFeatureBehaviour>();
			}

            if (featureList == null)
                featureList = new List<CustomFeatureBehaviour>();
            else
                featureList.Clear();
		}

		public override void Run(VectorEntity ve, UnityTile tile)
		{
            Vector3 posOffset = new Vector3(0, prefab.GetComponent<BoxCollider>().size.y, 0);
            posOffset.Scale(prefab.transform.localScale * .5f);

            ve.GameObject.transform.position = ve.GameObject.transform.position + posOffset;

            if (_features.ContainsKey(ve.GameObject))
			{
                _features[ve.GameObject].Initialize(ve);
                ve.GameObject.SetActive(isShowingFeatures);
            }
			else
			{                
                _tempFeature = ve.GameObject.AddComponent<CustomFeatureBehaviour>();
                _features.Add(ve.GameObject, _tempFeature);
				_tempFeature.Initialize(ve);
                ve.GameObject.SetActive(isShowingFeatures);
                featureList.Add(_tempFeature);
            }
		}
	}
}