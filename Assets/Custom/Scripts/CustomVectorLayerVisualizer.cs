using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Unity.MeshGeneration.Modifiers;
using Mapbox.VectorTile;
using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.MeshGeneration.Filters;
using Mapbox.Unity.MeshGeneration.Interfaces;
using Mapbox.Unity.MeshGeneration.Components;

public class CustomVectorLayerVisualizer : VectorLayerVisualizer
{
    public void SetProperties(VectorSubLayerProperties properties, LayerPerformanceOptions performanceOptions)
    {
        base.SetProperties(properties, performanceOptions);   
    }
}
