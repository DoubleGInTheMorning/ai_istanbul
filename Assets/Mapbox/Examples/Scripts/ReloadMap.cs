namespace Mapbox.Examples
{
	using Mapbox.Geocoding;
	using UnityEngine.UI;
	using Mapbox.Unity.Map;
	using UnityEngine;
	using System;
	using System.Collections;

	public class ReloadMap : MonoBehaviour
	{	
        [SerializeField]
        Camera _camera;
        [SerializeField]
        CameraMovement _cameraMovement;

		Vector3 _cameraStartPos;
        [SerializeField]
		AbstractMap _map;

		[SerializeField]
		CustomForwardGeocodeUserInput _forwardGeocoder;

        [SerializeField]
        Button _zoomPlus;
        [SerializeField]
        Button _zoomMinus;
        private float _currentZoom;

		Coroutine _reloadRoutine;

		WaitForSeconds _wait;

        void Awake()
		{
			_camera = Camera.main;
			_cameraStartPos = _camera.transform.position;
            //_forwardGeocoder.OnGeocoderResponse += ForwardGeocoder_OnGeocoderResponse;
            _zoomPlus.onClick.AddListener(OnZoomPlus);
            _zoomMinus.onClick.AddListener(OnZoomSub);

			_wait = new WaitForSeconds(.3f);
		}

        void ForwardGeocoder_OnGeocoderResponse(ForwardGeocodeResponse response)
		{
			if(response == null)
			{
				return;
			}
			_camera.transform.position = _cameraStartPos;
		}

        void Update()
        {
                   
        }

        public void OnZoomPlus()
        {
            if (_cameraMovement.Zoom < _cameraMovement._maxZoom)
            {
                _cameraMovement.Zoom += 1;
                Reload(_cameraMovement.Zoom);
            }
        }

        public void OnZoomSub()
        {
            if (_cameraMovement.Zoom > _cameraMovement._minZoom)
            { 
                _cameraMovement.Zoom -= 1;
                Reload(_cameraMovement.Zoom);
            }
        }

        public void Reload(float value)
		{
            _map.UpdateMap(_map.WorldToGeoPosition(_camera.transform.position), value);
            _camera.transform.position = _cameraStartPos;
        }

        public void ResetZoom(float value)
        {
            _camera.transform.position = _cameraStartPos;
            _map.UpdateMap(_map.CenterLatitudeLongitude, value);            
        }

		IEnumerator ReloadAfterDelay(int zoom)
		{
			yield return _wait;
			_camera.transform.position = _cameraStartPos;
			_map.UpdateMap(_map.CenterLatitudeLongitude, zoom);
			_reloadRoutine = null;
		}
	}
}