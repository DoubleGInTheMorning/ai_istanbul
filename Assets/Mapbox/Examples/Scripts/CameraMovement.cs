namespace Mapbox.Examples
{
	using UnityEngine;
	using UnityEngine.EventSystems;
    using Mapbox.Unity.Map;

    public class CameraMovement : MonoBehaviour
    {
        [SerializeField]
        float _panSpeed;

        [SerializeField]
        float _zoomSpeed;
        [SerializeField]
        [Range(0.01f, 0.05f)]
        float _touchZoomSpeed;
        
        [SerializeField]
        Camera _referenceCamera;

        [SerializeField]
        Camera _mainCamera;

        Quaternion _originalRotation;
        Vector3 _origin;
        Vector3 _delta;
        bool _shouldDrag;
        bool _canMove;

        [Header("Zoom")]
        public float _minZoom;
        public float _maxZoom;
        public float _defaultZoom;
        public float _showFeaturesZoom;
        public float ShowFeaturesZoom
        {
            get { return _map._showFeaturesZoom; }
            set
            {
                if (value >= _minZoom && value <= _maxZoom)
                {
                    _showFeaturesZoom = value;
                    _map._showFeaturesZoom = value;
                }
            }
        }


        [Header("Map")]
        public AbstractMap _map;
        public ReloadMap _reloadMap;
        public Utils.Vector2d MinLongLat;
        public Utils.Vector2d MaxLongLat;


        private float _zoomDrag;
        private float _zoom;
        public float Zoom
        {
            get { return _zoom;}
            set { _zoom = Mathf.Clamp(value, _minZoom, _maxZoom); }
        }

        void Awake()
        {
            _mainCamera.farClipPlane = transform.position.y + 1.0f;

            _zoomDrag = 0.0f;
            _zoom = _defaultZoom;
            _map.SetZoom(_zoom);
            _map._showFeaturesZoom = _showFeaturesZoom;

            _originalRotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);

            if (_referenceCamera == null)
            {
                _referenceCamera = GetComponent<Camera>();
                if (_referenceCamera == null)
                {
                    throw new System.Exception("You must have a reference camera assigned!");
                }
            }
        }

        private void Start()
        {     
        }

        void HandleTouch()
		{
			//pinch to zoom. 
			switch (Input.touchCount)
			{
				case 1:
					{
						HandleMouseAndKeyBoard();
					}
					break;
				case 2:
					{
						// Store both touches.
						Touch touchZero = Input.GetTouch(0);
						Touch touchOne = Input.GetTouch(1);

						// Find the position in the previous frame of each touch.
						Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
						Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

						// Find the magnitude of the vector (the distance) between the touches in each frame.
						float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
						float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

						// Find the difference in the distances between each frame.
						_zoomDrag += _touchZoomSpeed * (touchDeltaMag - prevTouchDeltaMag);
                        Debug.Log("Touch delta" + (touchDeltaMag - prevTouchDeltaMag));                        
                    }
                    if (Mathf.Abs(_zoomDrag) > .5f)
                    {
                        ZoomMapUsingTouchOrMouse(_zoom + _zoomDrag);
                        _zoomDrag = 0.0f;
                    }
                    break;
				default:
					break;
			}
		}

		void ZoomMapUsingTouchOrMouse(float value)
		{
            //var y = Mathf.Abs(zoomFactor - _zoom) * _zoomSpeed;
            //transform.localPosition += (transform.forward * y);
            UpdateZoom(value);
        }

		void HandleMouseAndKeyBoard()
		{
			if (Input.GetMouseButton(0) /*&& !EventSystem.current.IsPointerOverGameObject()*/)
			{
				var mousePosition = Input.mousePosition;
				mousePosition.z = _referenceCamera.transform.localPosition.y;
				_delta = _referenceCamera.ScreenToWorldPoint(mousePosition) - _referenceCamera.transform.localPosition;
				_delta.y = 0f;
				if (_shouldDrag == false)
				{
                    _shouldDrag = true;
					_origin = _referenceCamera.ScreenToWorldPoint(mousePosition);
				}
			}
			else
			{
				_shouldDrag = false;
			}

			if (_shouldDrag == true)
			{
                var offset = _origin - _delta;
				offset.y = transform.localPosition.y;				
                MoveTo(offset);
			}
			else
			{
                var x = Input.GetAxis("Horizontal");
				var z = Input.GetAxis("Vertical");

                if (Input.GetAxis("Mouse ScrollWheel") != 0 )
                    ZoomMapUsingTouchOrMouse(_zoom + Input.GetAxis("Mouse ScrollWheel") * _zoomSpeed);

                if(x != 0.0f || z != 0.0f)
                    MoveTo(transform.localPosition + (_originalRotation * new Vector3(x * _panSpeed, 0, z * _panSpeed)));
            }

        }

        public void UpdateZoom(float value)
        {
            if (value >= _minZoom && value <= _maxZoom)
            {
                _zoom = value;
                _reloadMap.Reload(_zoom);
            }
        }

        public void ResetZoom()
        {
            _zoom = Mathf.Clamp(_defaultZoom, _minZoom, _maxZoom);
            _reloadMap.ResetZoom(_zoom);
        }

        public void MoveTo(Vector3 newPosition)
        {
            if (!_canMove)
                return;

            Vector3[] frustumCorners = new Vector3[4];
            _mainCamera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), _mainCamera.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);

            for(int i = 0; i < 4; i++)
            {
                Vector3 worldSpaceCorner = frustumCorners[i];
                worldSpaceCorner.y = frustumCorners[i].z;
                worldSpaceCorner.z = frustumCorners[i].y;
                worldSpaceCorner -= newPosition;

                var latlong = _map.WorldToGeoPosition(worldSpaceCorner);
                if (latlong.x > MaxLongLat.x || latlong.x < MinLongLat.x ||
                   latlong.y > MaxLongLat.y || latlong.y < MinLongLat.y)
                    return;
            }
            transform.localPosition = newPosition;
        }

        public void SetCanMove(bool flag)
        {
            _canMove = flag;
        }
        
		void LateUpdate()
		{            
            if (Input.touchSupported && Input.touchCount > 0)
				HandleTouch();
            else if (_canMove)
                HandleMouseAndKeyBoard();
		}
	}
}